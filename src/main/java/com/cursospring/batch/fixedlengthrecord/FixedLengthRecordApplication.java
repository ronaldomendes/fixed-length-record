package com.cursospring.batch.fixedlengthrecord;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableBatchProcessing
@ComponentScan(basePackages = {"com.cursospring.batch.fixedlengthrecord"})
public class FixedLengthRecordApplication {

    public static void main(String[] args) {
        SpringApplication.run(FixedLengthRecordApplication.class, args);
    }

}
