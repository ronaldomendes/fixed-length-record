package com.cursospring.batch.fixedlengthrecord.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.file.LineCallbackHandler;

@Slf4j
public class SkipRecordCallback implements LineCallbackHandler {

    @Override
    public void handleLine(String line) {
        log.info("First record data: {}", line);
    }
}
